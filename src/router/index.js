import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
        path: '/index',
        name: 'index',
        component: () =>
            import ('../views/index/Index.vue'),
        children: [{
                path: '/article',
                name: 'article',
                component: () =>
                    import ('../views/article/Index.vue')
            },
            {
                path: '/addarticle',
                name: 'addarticle',
                component: () =>
                    import ('../views/article/AddArticle.vue')
            },
            {
                path: '/editarticle',
                name: 'editarticle',
                component: () =>
                    import ('../views/article/EditArticle.vue')
            },
            {
                path: '/tags',
                name: 'tags',
                component: () =>
                    import ('../views/tags/Index.vue')
            },
            {
                path: '/category',
                name: 'category',
                component: () =>
                    import ('../views/category/Index.vue')
            },
            {
                path: '/picture',
                name: 'picture',
                component: () =>
                    import ('../views/picture/Index.vue')
            },
            {
                path: '/comment',
                name: 'comment',
                component: () =>
                    import ('../views/comment/Index.vue')
            },
            {
                path: '/message',
                name: 'message',
                component: () =>
                    import ('../views/message/Index.vue')
            },
            {
                path: '/web',
                name: 'web',
                component: () =>
                    import ('../views/web/Index.vue')
            },
        ]
    },
    {
        path: '/',
        name: 'login',
        component: () =>
            import ('../views/login/Index.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

// router.beforeEach((to, from, next) => {
//     let token = localStorage.getItem('token') || ''
//     let time = token.time
//     let timenow = Date.now()
//     console.log(time)
//     if (token.token) {
//         next()
//     } else {
//         if (to.path == '/login') {
//             next()
//         } else {
//             next({ path: '/login' })
//         }
//     }
// })
export default router