import { request } from './request'
export function _commentList(params) {
    return request({
        url: '/commentlist',
        method: 'get',
        params: params
    })
}
export function _addComment(params) {
    return request({
        url: '/addcomment',
        method: 'post',
        params: params
    })
}
export function _deleteComment(params) {
    return request({
        url: '/deletecomment',
        method: 'post',
        params: params
    })
}
// export function _editComment(params) {
//     return request({
//         url: '/editcomment',
//         method: 'post',
//         params: params
//     })
// }