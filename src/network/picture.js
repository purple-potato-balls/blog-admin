import { request } from './request'
export function _addPicture(params) {
    return request({
        url: '/addpicture',
        method: 'post',
        params: params
    })
}