import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        token: '',
        userinfo: {}
    },
    mutations: {
        // getToken(state, value) {
        //     state.token = value
        // },
        getUserInfo(state, value) {
            state.token = value.token
            state.userinfo = value.results
        },
        layout(state, value) {
            state.token = ''
            state.userinfo = {}
        }
    }
})