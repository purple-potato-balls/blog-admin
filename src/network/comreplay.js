import { request } from './request'
export function _comReplayList(params) {
    return request({
        url: '/comreplaylist',
        method: 'get',
        params: params
    })
}
export function _addComReplay(params) {
    return request({
        url: '/addcomreplay',
        method: 'post',
        params: params
    })
}
export function _deleteComReplay(params) {
    return request({
        url: '/deletecomreplay',
        method: 'post',
        params: params
    })
}