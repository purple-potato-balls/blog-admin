import { request } from './request'
export function _tagsList(params) {
    return request({
        url: '/taglist',
        method: 'get',
        params: params
    })
}
export function _addTags(params) {
    return request({
        url: '/addtag',
        method: 'post',
        params: params
    })
}
export function _deleteTags(params) {
    return request({
        url: '/deletetag',
        method: 'post',
        params: params
    })
}
export function _editTags(params) {
    return request({
        url: '/edittag',
        method: 'post',
        params: params
    })
}

export function _findTagById(params) {
    return request({
        url: '/fingtagbyid',
        method: 'get',
        params: params
    })
}