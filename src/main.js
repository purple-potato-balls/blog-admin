import Vue from 'vue'
import App from './App.vue'
// 导入路由
import router from './router'
// 导入element ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// 导入编辑器
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'

// 导入公共样式
import './style/common.css';
import store from './store';
Vue.config.productionTip = false

Vue.use(ElementUI);
Vue.use(mavonEditor)
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')