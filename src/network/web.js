import { request } from './request'
export function _addWebSite(params) {
    return request({
        url: '/addweb',
        method: 'post',
        params: params
    })
}
export function _webList(params) {
    return request({
        url: '/weblist',
        method: 'get',
        params: params
    })
}