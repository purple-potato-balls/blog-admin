import { request } from './request'
export function _login(params) {
    return request({
        url: '/login',
        method: 'post',
        params: params
    })
}
export function _register(params) {
    return request({
        url: '/reguser',
        method: 'post',
        params: params
    })
}
export function _getUserById(params) {
    return request({
        url: '/getuserbyid',
        method: 'get',
        params: params
    })
}
export function _getUser() {
    return request({
        url: '/getuserlist',
        method: 'get'
    })
}