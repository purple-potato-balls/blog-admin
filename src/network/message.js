import { request } from './request'
export function _messageList(params) {
    return request({
        url: '/messagelist',
        method: 'get',
        params: params
    })
}
export function _addMessage(params) {
    return request({
        url: '/addmessage',
        method: 'post',
        params: params
    })
}
export function _addMreplay(params) {
    return request({
        url: '/addmreplay',
        method: 'post',
        params: params
    })
}

export function _deleteMessage(params) {
    return request({
        url: '/deletemessage',
        method: 'post',
        params: params
    })
}