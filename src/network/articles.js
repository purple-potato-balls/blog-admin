import { request } from './request'
export function _articlesList(params) {
    return request({
        url: '/articlelist',
        method: 'get',
        params: params
    })
}
export function _addArticle(params) {
    return request({
        url: '/addarticle',
        method: 'post',
        params: params
    })
}
export function _deleteArticle(params) {
    return request({
        url: '/deletearticle',
        method: 'post',
        params: params
    })
}
export function _editArticle(params) {
    return request({
        url: '/editarticle',
        method: 'post',
        params: params
    })
}