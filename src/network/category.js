import { request } from './request'
export function _categoryList(params) {
    return request({
        url: '/catelist',
        method: 'get',
        params: params
    })
}
export function _addCategory(params) {
    return request({
        url: '/addcate',
        method: 'post',
        params: params
    })
}
export function _deleteCategory(params) {
    return request({
        url: '/deletecate',
        method: 'post',
        params: params
    })
}
export function _editCategory(params) {
    return request({
        url: '/editcate',
        method: 'post',
        params: params
    })
}